﻿using System.Linq;
using luizalabs_wishes_manager.Api.Product.Service;
using luizalabs_wishes_manager.Api.User.Service;
using luizalabs_wishes_manager.Api.Wish.Service;
using luizalabs_wishes_manager.Exception;
using luizalabs_wishes_manager.Persistence;
using luizalabs_wishes_manager.Persistence.Product.Repository;
using luizalabs_wishes_manager.Persistence.User.Repository;
using luizalabs_wishes_manager.Persistence.Wish.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace luizalabs_wishes_manager
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var sqlConnection = Configuration["ConnectionString"];
            services.AddDbContext<AppContext>(options => options.UseMySql(sqlConnection));
            SetupDependencyInjection(services);
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware(typeof(ExceptionHandlerMiddleware));
            app.UseHttpsRedirection();
            app.UseMvc();
        }

        private void SetupDependencyInjection(IServiceCollection services)
        {
            services.AddScoped<AppContext, AppContext>();

            services.AddScoped<IUserBuilder, UserBuilder>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IProductBuilder, ProductBuilder>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddScoped<IWishBuilder, WishBuilder>();
            services.AddScoped<IWishService, WishService>();
            services.AddScoped<IWishRepository, WishRepository>();
        }
    }
}