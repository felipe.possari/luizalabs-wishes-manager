# Luizalabs Wishes Manager

Wish list api

## Requirements

[.NET Core 2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2)

## Usage

1.  Check your database user and password in the file [appsettings.json](https://gitlab.com/felipe.possari/luizalabs-wishes-manager/blob/master/appsettings.json#L8);
2.  Run the commands in the console `dotnet ef database update` and `dotnet run`;