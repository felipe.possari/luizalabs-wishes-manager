using System.Collections.Generic;
namespace luizalabs_wishes_manager.Exception.Model
{
    public class ResponseError
    {
        public List<BaseError> Errors { get; set; } = new List<BaseError>();
        
        public class Builder
        {
            private List<BaseError> errors = new List<BaseError>();

            public Builder AddError(BaseError error)
            {
                this.errors.Add(error);
                return this;
            }

            public Builder AddErrors(List<BaseError> errors)
            {
                this.errors = errors;
                return this;
            }

            public ResponseError Build()
            {
                return new ResponseError{
                    Errors = this.errors
                };
            }
        }
    }
}