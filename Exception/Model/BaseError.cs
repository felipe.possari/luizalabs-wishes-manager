namespace luizalabs_wishes_manager.Exception.Model
{
    public class BaseError
    {
        public string Code { get; set; }
        public string Message { get; set; }

        public class Builder
        {
            private BaseError baseError = new BaseError();

            public Builder Code(string code)
            {
                baseError.Code = code;
                return this;
            }

            public Builder Message(string message)
            {
                baseError.Message = message;
                return this;
            }

            public BaseError Build()
            {
                return baseError;
            }
        }
    }
}