namespace luizalabs_wishes_manager.Exception.Model
{
    public class SystemOperationExceptionCode
    {
        private SystemOperationExceptionCode(string value)
        {
            Value = value;
        }

        public string Value { get; set; }

        public static SystemOperationExceptionCode UserApi { get { return new SystemOperationExceptionCode("001"); } }
        public static SystemOperationExceptionCode ProductApi { get { return new SystemOperationExceptionCode("002"); } }
        public static SystemOperationExceptionCode WishApi { get { return new SystemOperationExceptionCode("003"); } }
    }
}