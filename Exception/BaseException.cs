using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using luizalabs_wishes_manager.Exception.Model;

namespace luizalabs_wishes_manager.Exception
{
    public class BaseException : System.Exception
    {
        private readonly string SystemCode = "001";
        private readonly SystemOperationExceptionCode operationExceptionCode;
        private readonly String reasonCode;
        private readonly String message;
        private readonly HttpStatusCode httpStatusCode;

        public String GetErrorCode() => new StringBuilder().Append(SystemCode).Append(operationExceptionCode.Value).Append(reasonCode).ToString();

        public HttpStatusCode GetHttpStatusCode() => this.httpStatusCode;

        public BaseException(String message, String reasonCode, SystemOperationExceptionCode systemOperationExceptionCode) : base(message)
        {
            this.reasonCode = reasonCode;
            this.message = message;
            this.operationExceptionCode = systemOperationExceptionCode;
        }

        public BaseException(String message, String reasonCode, HttpStatusCode httpStatusCode, SystemOperationExceptionCode systemOperationExceptionCode) : base(message)
        {
            this.reasonCode = reasonCode;
            this.httpStatusCode = httpStatusCode;
            this.message = message;
            this.operationExceptionCode = systemOperationExceptionCode;
        }

        public ResponseError CreateResponseError()
        {
            List<BaseError> errors = new List<BaseError>(){
                BuildSystemError()
            };
            return new ResponseError.Builder().AddErrors(errors).Build();
        }

        private BaseError BuildSystemError()
        {
            return new BaseError.Builder()
                .Code(this.GetErrorCode())
                .Message(this.message)
                .Build();
        }
    }
}