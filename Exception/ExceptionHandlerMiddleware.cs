using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using luizalabs_wishes_manager.Exception.Model;

namespace luizalabs_wishes_manager.Exception
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (System.Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, System.Exception ex)
        {
            if (ex is BaseException)
            {
                BaseException baseEx = ex as BaseException;
                var responseError = baseEx.CreateResponseError();
                context.Response.ContentType = "application/json";
                context.Response.StatusCode = baseEx.GetHttpStatusCode().GetHashCode();
                return context.Response.WriteAsync(SerializeJson(responseError));
            }

            var responseUnkwownError = BuildUnknowResponseError(ex);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = HttpStatusCode.InternalServerError.GetHashCode();
            return context.Response.WriteAsync(SerializeJson(responseUnkwownError));
        }

        private static ResponseError BuildUnknowResponseError(System.Exception ex)
        {
            var error = new BaseError.Builder()
                .Code(HttpStatusCode.InternalServerError.GetHashCode().ToString())
                .Message(ex.Message)
                .Build();
            return new ResponseError.Builder()
                .AddError(error)
                .Build();
        }

        private static string SerializeJson(object responseError)
        {
            return JsonConvert.SerializeObject(responseError, new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                    {
                        OverrideSpecifiedNames = false
                    }, 
                    IgnoreSerializableInterface = true 
                }
            });
        }
    }
}