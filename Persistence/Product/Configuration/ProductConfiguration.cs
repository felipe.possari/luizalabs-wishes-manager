using luizalabs_wishes_manager.Persistence.Product.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace luizalabs_wishes_manager.Persistence.Product.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.HasKey(product => product.Id);

            builder.Property(Product => Product.Name)
                .IsRequired();
        }
    }
}