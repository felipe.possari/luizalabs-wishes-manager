namespace luizalabs_wishes_manager.Persistence.Product.Model
{
    public class ProductEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public class Builder
        {
            private int id;
            private string name;

            public Builder Id(int id)
            {
                this.id = id;
                return this;
            }

            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public ProductEntity Build()
            {
                return new ProductEntity
                {
                    Id = id,
                        Name = name
                };
            }
        }
    }
}