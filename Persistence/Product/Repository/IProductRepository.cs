using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Persistence.Product.Model;

namespace luizalabs_wishes_manager.Persistence.Product.Repository
{
    public interface IProductRepository
    {
         Task<ProductEntity> Save(ProductEntity productEntity);
         Task<List<ProductEntity>> FindProducts(int pageSize, int page);
         Task<ProductEntity> FindProductByName(string name);
         Task<ProductEntity> FindProductById(int id);
    }
}