using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Persistence.Product.Model;
using Microsoft.EntityFrameworkCore;

namespace luizalabs_wishes_manager.Persistence.Product.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly AppContext appContext;

        public ProductRepository(AppContext appContext)
        {
            this.appContext = appContext;
        }

        public async Task<List<ProductEntity>> FindProducts(int pageSize, int page)
        {
            return await this.appContext.Set<ProductEntity>()
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<ProductEntity> Save(ProductEntity productEntity)
        {
            await this.appContext.AddAsync(productEntity);
            await this.appContext.SaveChangesAsync();
            return productEntity;
        }

        public async Task<ProductEntity> FindProductByName(string name)
        {
            return await this.appContext.Set<ProductEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(product => product.Name.ToLower() == name.ToLower());
        }

        public async Task<ProductEntity> FindProductById(int id)
        {
            return await this.appContext.Set<ProductEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(product => product.Id == id);
        }
    }
}