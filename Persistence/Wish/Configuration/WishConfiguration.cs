using luizalabs_wishes_manager.Persistence.Wish.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace luizalabs_wishes_manager.Persistence.Wish.Configuration
{
    public class WishConfiguration : IEntityTypeConfiguration<WishEntity>
    {
        public void Configure(EntityTypeBuilder<WishEntity> builder)
        {
            builder.HasKey(p => new { p.ProductId, p.UserId });

            builder.HasOne(p => p.Product)
                .WithMany()
                .HasForeignKey(p => p.ProductId);

            builder.HasOne(p => p.User)
                .WithMany(p=> p.Wishes)
                .HasForeignKey(p => p.UserId);
        }
    }
}