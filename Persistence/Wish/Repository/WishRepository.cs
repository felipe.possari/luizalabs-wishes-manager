using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Persistence.Wish.Model;
using Microsoft.EntityFrameworkCore;

namespace luizalabs_wishes_manager.Persistence.Wish.Repository
{
    public class WishRepository : IWishRepository
    {
        private readonly AppContext appContext;

        public WishRepository(AppContext appContext)
        {
            this.appContext = appContext;
        }

        public async Task<bool> Delete(WishEntity wishEntity)
        {
            appContext.Set<WishEntity>().Remove(wishEntity);
            return await appContext.SaveChangesAsync() > 0;
        }

        public async Task<WishEntity> FindWishByUserIdAndProductId(int userId, int productId)
        {
            return await appContext.Set<WishEntity>()
                .FirstOrDefaultAsync(w => w.UserId == userId && w.ProductId == productId);
        }

        public async Task<List<WishEntity>> FindWishesByUserId(int userId, int pageSize, int page)
        {
            return await appContext.Set<WishEntity>()
                .Include(p => p.Product)
                .Where(w => w.UserId == userId)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<WishEntity>> Save(List<WishEntity> wishEntities)
        {
            await appContext.Set<WishEntity>().AddRangeAsync(wishEntities);
            await appContext.SaveChangesAsync();
            return wishEntities;
        }
    }
}