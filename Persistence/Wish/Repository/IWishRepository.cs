using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Persistence.Wish.Model;

namespace luizalabs_wishes_manager.Persistence.Wish.Repository
{
    public interface IWishRepository
    {
         Task<List<WishEntity>> Save(List<WishEntity> wishEntities);
         Task<List<WishEntity>> FindWishesByUserId(int userId, int pageSize, int page);
         Task<bool> Delete(WishEntity wishEntity);
         Task<WishEntity> FindWishByUserIdAndProductId(int userId, int productId);
    }
}