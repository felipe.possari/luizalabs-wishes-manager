using luizalabs_wishes_manager.Persistence.Product.Model;
using luizalabs_wishes_manager.Persistence.User.Model;

namespace luizalabs_wishes_manager.Persistence.Wish.Model
{
    public class WishEntity
    {
        public int UserId { get; set; }
        public int ProductId { get; set; }
        public virtual UserEntity User { get; set; }
        public virtual ProductEntity Product { get; set; }

        public class Builder
        {
            private int userId;
            private int productId;
            private ProductEntity productEntity;
            private UserEntity userEntity;

            public Builder UserId(int userId)
            {
                this.userId = userId;
                return this;
            }

            public Builder ProductId(int productId)
            {
                this.productId = productId;
                return this;
            }

            public Builder Product(ProductEntity productEntity)
            {
                this.productEntity = productEntity;
                return this;
            }

            public Builder User(UserEntity userEntity)
            {
                this.userEntity = userEntity;
                return this;
            }

            public WishEntity Build()
            {
                return new WishEntity
                {
                    UserId = userId,
                        ProductId = productId,
                        Product = productEntity,
                        User = userEntity
                };
            }
        }
    }
}