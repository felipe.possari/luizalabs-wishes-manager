using System.Collections;
using System.Collections.Generic;
using luizalabs_wishes_manager.Persistence.Wish.Model;

namespace luizalabs_wishes_manager.Persistence.User.Model
{
    public class UserEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public ICollection<WishEntity> Wishes { get; set; }

        public class Builder
        {
            private int id;
            private string name;
            private string email;
            private ICollection<WishEntity> wishes = new List<WishEntity>();

            public Builder Id(int id)
            {
                this.id = id;
                return this;
            }

            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public Builder Email(string email)
            {
                this.email = email;
                return this;
            }

            public Builder Wishes(IList<WishEntity> wishes)
            {
                this.wishes = wishes;
                return this;
            }

            public UserEntity Build()
            {
                return new UserEntity
                {
                    Id = id,
                        Email = email,
                        Name = name,
                        Wishes = wishes
                };
            }
        }
    }
}