using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Persistence.User.Model;

namespace luizalabs_wishes_manager.Persistence.User.Repository
{
    public interface IUserRepository
    {
         Task<UserEntity> Save(UserEntity userEntity);
         Task<List<UserEntity>> FindUsers(int pageSize, int page);
         Task<UserEntity> FindUserByEmail(string email);
         Task<UserEntity> FindUserById(int id);
    }
}