using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Persistence.User.Model;
using Microsoft.EntityFrameworkCore;

namespace luizalabs_wishes_manager.Persistence.User.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly AppContext appContext;

        public UserRepository(AppContext appContext)
        {
            this.appContext = appContext;
        }

        public async Task<List<UserEntity>> FindUsers(int pageSize, int page)
        {
            return await this.appContext.Set<UserEntity>()
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<UserEntity> Save(UserEntity userEntity)
        {
            await this.appContext.AddAsync(userEntity);
            await this.appContext.SaveChangesAsync();
            return userEntity;
        }

        public async Task<UserEntity> FindUserByEmail(string email)
        {
            return await this.appContext.Set<UserEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(user => user.Email == email);
        }

        public async Task<UserEntity> FindUserById(int id)
        {
            return await this.appContext.Set<UserEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(user => user.Id == id);
        }
    }
}