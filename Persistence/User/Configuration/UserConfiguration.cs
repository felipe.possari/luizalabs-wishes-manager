using luizalabs_wishes_manager.Persistence.User.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace luizalabs_wishes_manager.Persistence.User.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.HasKey(user => user.Id);

            builder.Property(user => user.Email)
                .IsRequired();

            builder.Property(user => user.Name)
                .IsRequired();
        }
    }
}