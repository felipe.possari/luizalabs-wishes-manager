using luizalabs_wishes_manager.Persistence.Product.Configuration;
using luizalabs_wishes_manager.Persistence.Product.Model;
using luizalabs_wishes_manager.Persistence.User.Configuration;
using luizalabs_wishes_manager.Persistence.User.Model;
using luizalabs_wishes_manager.Persistence.Wish.Configuration;
using luizalabs_wishes_manager.Persistence.Wish.Model;
using Microsoft.EntityFrameworkCore;

namespace luizalabs_wishes_manager.Persistence
{
    public class AppContext : DbContext
    {
        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new WishConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public UserEntity Users { get; set; }
        public ProductEntity Products { get; set; }
        public WishEntity Wishes { get; set; }
    }
}