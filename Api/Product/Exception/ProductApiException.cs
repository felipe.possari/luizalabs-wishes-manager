using luizalabs_wishes_manager.Exception;
using luizalabs_wishes_manager.Exception.Model;

namespace luizalabs_wishes_manager.Api.Product.Exception
{
    public class ProductApiException : BaseException
    {
        public ProductApiException(ProductApiErrorReason ProductApiErrorReason) : base(
            ProductApiErrorReason.Message,
            ProductApiErrorReason.Code,
            ProductApiErrorReason.HttpStatusCode,
            SystemOperationExceptionCode.ProductApi) { }
    }
}