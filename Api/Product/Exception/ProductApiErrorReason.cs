using System.Net;

namespace luizalabs_wishes_manager.Api.Product.Exception
{
    public class ProductApiErrorReason
    {
        private readonly string code;
        private readonly string message;
        private readonly HttpStatusCode httpStatusCode;

        public string Code { get { return code; } }
        public string Message { get { return message; } }
        public HttpStatusCode HttpStatusCode { get { return httpStatusCode; } }

        private ProductApiErrorReason(string code, string message, HttpStatusCode httpStatusCode)
        {
            this.code = code;
            this.message = message;
            this.httpStatusCode = httpStatusCode;
        }

        public static ProductApiErrorReason NameInvalid { get { return new ProductApiErrorReason("001", "Name cannot be null or empty.", HttpStatusCode.BadRequest); } }
        public static ProductApiErrorReason ProductExists { get { return new ProductApiErrorReason("002", "Product already registered.", HttpStatusCode.BadRequest); } }
    }
}