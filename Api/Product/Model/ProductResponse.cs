namespace luizalabs_wishes_manager.Api.Product.Model
{
    public class ProductResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public class Builder
        {
            private int id;
            private string name;

            public Builder Id(int id)
            {
                this.id = id;
                return this;
            }

            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public ProductResponse Build()
            {
                return new ProductResponse
                {
                    Id = id,
                        Name = name
                };
            }
        }
    }
}