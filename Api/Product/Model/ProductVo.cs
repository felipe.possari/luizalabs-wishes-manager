namespace luizalabs_wishes_manager.Api.Product.Model
{
    public class ProductVo
    {
        public string Name { get; private set; }

        public class Builder
        {
            private string name;
            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public ProductVo Build()
            {
                return new ProductVo
                {
                    Name = name
                };
            }
        }
    }
}