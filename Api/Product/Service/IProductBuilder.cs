using System.Collections.Generic;
using luizalabs_wishes_manager.Api.Product.Model;
using luizalabs_wishes_manager.Persistence.Product.Model;

namespace luizalabs_wishes_manager.Api.Product.Service
{
    public interface IProductBuilder
    {
         ProductVo BuildVo(ProductRequest productRequest);

         ProductEntity BuildEntity(ProductVo productVo);

         ProductResponse BuildResponse(ProductEntity productEntity);

         List<ProductResponse> BuildResponseList(List<ProductEntity> products);
    }
}