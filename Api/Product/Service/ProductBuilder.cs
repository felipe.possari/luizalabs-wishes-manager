using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using luizalabs_wishes_manager.Api.Product.Exception;
using luizalabs_wishes_manager.Api.Product.Model;
using luizalabs_wishes_manager.Persistence.Product.Model;

namespace luizalabs_wishes_manager.Api.Product.Service
{
    public class ProductBuilder : IProductBuilder
    {
        public ProductEntity BuildEntity(ProductVo productVo)
        {
            return new ProductEntity.Builder()
                .Name(productVo.Name)
                .Build();
        }

        public ProductResponse BuildResponse(ProductEntity productEntity)
        {
            return new ProductResponse.Builder()
                .Id(productEntity.Id)
                .Name(productEntity.Name)
                .Build();
        }

        public List<ProductResponse> BuildResponseList(List<ProductEntity> products)
        {
            List<ProductResponse> productsResponse = new List<ProductResponse>();
            products.ForEach(product => productsResponse.Add(BuildResponse(product)));
            return productsResponse;
        }

        public ProductVo BuildVo(ProductRequest productRequest)
        {
            ValidateName(productRequest.Name);
            return new ProductVo.Builder()
                .Name(productRequest.Name)
                .Build();
        }

        private void ValidateName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ProductApiException(ProductApiErrorReason.NameInvalid);
            }
        }
    }
}