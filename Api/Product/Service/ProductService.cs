using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.Product.Exception;
using luizalabs_wishes_manager.Api.Product.Model;
using luizalabs_wishes_manager.Persistence.Product.Model;
using luizalabs_wishes_manager.Persistence.Product.Repository;

namespace luizalabs_wishes_manager.Api.Product.Service
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository iProductRepository;
        private readonly IProductBuilder iProductBuilder;

        public ProductService(IProductRepository iProductRepository, IProductBuilder iProductBuilder)
        {
            this.iProductRepository = iProductRepository;
            this.iProductBuilder = iProductBuilder;
        }

        public async Task<List<ProductResponse>> GetProducts(int pageSize, int page)
        {
            var products = await this.iProductRepository.FindProducts(pageSize, page);
            return iProductBuilder.BuildResponseList(products);
        }

        public async Task<ProductResponse> Post(ProductVo productVo)
        {
            ValidateProduct(productVo.Name);
            ProductEntity productEntity = iProductBuilder.BuildEntity(productVo);
            productEntity = await iProductRepository.Save(productEntity);
            return iProductBuilder.BuildResponse(productEntity);
        }

        public void ValidateProduct(string name)
        {
            if (iProductRepository.FindProductByName(name).Result != null)
            {
                throw new ProductApiException(ProductApiErrorReason.ProductExists);
            }
        }
    }
}