using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.Product.Model;

namespace luizalabs_wishes_manager.Api.Product.Service
{
    public interface IProductService
    {
        Task<ProductResponse> Post(ProductVo productVo);
        Task<List<ProductResponse>> GetProducts(int pageSize, int page);
    }
}