using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.Product.Model;
using luizalabs_wishes_manager.Api.Product.Service;
using Microsoft.AspNetCore.Mvc;

namespace luizalabs_wishes_manager.Api.Product.Controller
{
    [Route("products")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService iProductService;
        private readonly IProductBuilder iProductBuilder;
        
        public ProductController(IProductService iProductService, IProductBuilder iProductBuilder)
        {
            this.iProductService = iProductService;
            this.iProductBuilder = iProductBuilder;
        }

        [HttpGet]
        public async Task<ActionResult<List<ProductResponse>>> Get(int pageSize = 10, int page = 1)
        {
            return Ok(await iProductService.GetProducts(pageSize, page));
        }

        [HttpPost]
        public async Task<ActionResult<ProductResponse>> Post([FromBody] ProductRequest productRequest)
        {
            ProductVo productVo = iProductBuilder.BuildVo(productRequest);
            var Product = await iProductService.Post(productVo);
            return CreatedAtAction(nameof(this.Post), new { id = Product.Id }, Product);
        }
    }
}