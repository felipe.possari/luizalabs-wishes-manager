using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using luizalabs_wishes_manager.Api.User.Exception;
using luizalabs_wishes_manager.Api.User.Model;
using luizalabs_wishes_manager.Persistence.User.Model;

namespace luizalabs_wishes_manager.Api.User.Service
{
    public class UserBuilder : IUserBuilder
    {
        public UserEntity BuildEntity(UserVo userVo)
        {
            return new UserEntity.Builder()
                .Email(userVo.Email)
                .Name(userVo.Name)
                .Build();
        }

        public UserResponse BuildResponse(UserEntity userEntity)
        {
            return new UserResponse.Builder()
                .Id(userEntity.Id)
                .Email(userEntity.Email)
                .Name(userEntity.Name)
                .Build();
        }

        public List<UserResponse> BuildResponseList(List<UserEntity> users)
        {
            List<UserResponse> usersResponse = new List<UserResponse>();
            users.ForEach(user => usersResponse.Add(BuildResponse(user)));
            return usersResponse;
        }

        public UserVo BuildVo(UserRequest userRequest)
        {
            ValidateName(userRequest.Name);
            ValidateEmail(userRequest.Email);
            return new UserVo.Builder()
                .Name(userRequest.Name)
                .Email(userRequest.Email)
                .Build();
        }

        private void ValidateName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new UserApiException(UserApiErrorReason.NameInvalid);
            }
        }

        private void ValidateEmail(string email)
        {
            if (string.IsNullOrEmpty(email) || !new EmailAddressAttribute().IsValid(email))
            {
                throw new UserApiException(UserApiErrorReason.EmailInvalid);
            }
        }
    }
}