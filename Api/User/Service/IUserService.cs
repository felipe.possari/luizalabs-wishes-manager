using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.User.Model;
using luizalabs_wishes_manager.Persistence.User.Model;

namespace luizalabs_wishes_manager.Api.User.Service
{
    public interface IUserService
    {
        Task<UserResponse> Post(UserVo userEntity);
        Task<List<UserResponse>> GetUsers(int pageSize, int page);
    }
}