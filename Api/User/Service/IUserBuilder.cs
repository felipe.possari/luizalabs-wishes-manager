using System.Collections.Generic;
using luizalabs_wishes_manager.Api.User.Model;
using luizalabs_wishes_manager.Persistence.User.Model;

namespace luizalabs_wishes_manager.Api.User.Service
{
    public interface IUserBuilder
    {
         UserVo BuildVo(UserRequest userRequest);

         UserEntity BuildEntity(UserVo userVo);

         UserResponse BuildResponse(UserEntity userEntity);

         List<UserResponse> BuildResponseList(List<UserEntity> users);
    }
}