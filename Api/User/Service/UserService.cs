using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.User.Exception;
using luizalabs_wishes_manager.Api.User.Model;
using luizalabs_wishes_manager.Persistence.User.Model;
using luizalabs_wishes_manager.Persistence.User.Repository;

namespace luizalabs_wishes_manager.Api.User.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository iUserRepository;
        private readonly IUserBuilder iUserBuilder;

        public UserService(IUserRepository iUserRepository, IUserBuilder iUserBuilder)
        {
            this.iUserRepository = iUserRepository;
            this.iUserBuilder = iUserBuilder;
        }

        public async Task<List<UserResponse>> GetUsers(int pageSize, int page)
        {
            var users = await this.iUserRepository.FindUsers(pageSize, page);
            return iUserBuilder.BuildResponseList(users);
        }

        public async Task<UserResponse> Post(UserVo userVo)
        {
            ValidateEmail(userVo.Email);
            UserEntity userEntity = iUserBuilder.BuildEntity(userVo);
            userEntity = await iUserRepository.Save(userEntity);
            return iUserBuilder.BuildResponse(userEntity);
        }

        public void ValidateEmail(string email)
        {
            if (iUserRepository.FindUserByEmail(email).Result != null)
            {
                throw new UserApiException(UserApiErrorReason.EmailExists);
            }
        }
    }
}