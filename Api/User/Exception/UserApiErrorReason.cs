using System.Net;

namespace luizalabs_wishes_manager.Api.User.Exception
{
    public class UserApiErrorReason
    {
        private readonly string code;
        private readonly string message;
        private readonly HttpStatusCode httpStatusCode;

        public string Code { get { return code; } }
        public string Message { get { return message; } }
        public HttpStatusCode HttpStatusCode { get { return httpStatusCode; } }

        private UserApiErrorReason(string code, string message, HttpStatusCode httpStatusCode)
        {
            this.code = code;
            this.message = message;
            this.httpStatusCode = httpStatusCode;
        }

        public static UserApiErrorReason NameInvalid { get { return new UserApiErrorReason("001", "Name cannot be null or empty.", HttpStatusCode.BadRequest); } }
        public static UserApiErrorReason EmailInvalid { get { return new UserApiErrorReason("002", "Email invalid.", HttpStatusCode.BadRequest); } }
        public static UserApiErrorReason EmailExists { get { return new UserApiErrorReason("003", "Email already registered.", HttpStatusCode.BadRequest); } }
    }
}