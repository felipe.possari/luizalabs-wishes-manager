using luizalabs_wishes_manager.Exception;
using luizalabs_wishes_manager.Exception.Model;

namespace luizalabs_wishes_manager.Api.User.Exception
{
    public class UserApiException : BaseException
    {
        public UserApiException(UserApiErrorReason userApiErrorReason) : base(
            userApiErrorReason.Message,
            userApiErrorReason.Code,
            userApiErrorReason.HttpStatusCode,
            SystemOperationExceptionCode.UserApi) { }
    }
}