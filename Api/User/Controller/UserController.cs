using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.User.Model;
using luizalabs_wishes_manager.Api.User.Service;
using Microsoft.AspNetCore.Mvc;

namespace luizalabs_wishes_manager.Api.User.Controller
{
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly IUserService iUserService;
        private readonly IUserBuilder IUserBuilder;
        
        public UserController(IUserService iUserService, IUserBuilder iUserBuilder)
        {
            this.iUserService = iUserService;
            this.IUserBuilder = iUserBuilder;
        }

        [HttpGet]
        public async Task<ActionResult<List<UserResponse>>> Get(int pageSize = 10, int page = 1)
        {
            return Ok(await iUserService.GetUsers(pageSize, page));
        }

        [HttpPost]
        public async Task<ActionResult<UserResponse>> Post([FromBody] UserRequest userRequest)
        {
            UserVo userVo = IUserBuilder.BuildVo(userRequest);
            var user = await iUserService.Post(userVo);
            return CreatedAtAction(nameof(this.Post), new { id = user.Id }, user);
        }
    }
}