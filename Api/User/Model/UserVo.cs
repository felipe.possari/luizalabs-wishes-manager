namespace luizalabs_wishes_manager.Api.User.Model
{
    public class UserVo
    {
        public string Name { get; private set; }
        public string Email { get; private set; }

        public class Builder
        {
            private string name;
            private string email;

            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public Builder Email(string email)
            {
                this.email = email;
                return this;
            }

            public UserVo Build()
            {
                return new UserVo
                {
                    Email = email, 
                    Name = name
                };
            }
        }
    }
}