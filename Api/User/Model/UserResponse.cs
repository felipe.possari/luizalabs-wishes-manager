namespace luizalabs_wishes_manager.Api.User.Model
{
    public class UserResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public class Builder
        {
            private int id;
            private string name;
            private string email;

            public Builder Id(int id)
            {
                this.id = id;
                return this;
            }

            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public Builder Email(string email)
            {
                this.email = email;
                return this;
            }

            public UserResponse Build()
            {
                return new UserResponse
                {
                    Id = id,
                        Email = email,
                        Name = name
                };
            }
        }
    }
}