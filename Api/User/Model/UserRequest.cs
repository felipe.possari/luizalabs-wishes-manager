namespace luizalabs_wishes_manager.Api.User.Model
{
    public class UserRequest
    {
        public string Name { get; set; } 
        public string Email { get; set; }
    }
}