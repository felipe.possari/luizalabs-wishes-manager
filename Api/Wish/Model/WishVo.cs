using System.Collections.Generic;

namespace luizalabs_wishes_manager.Api.Wish.Model
{
    public class WishVo
    {
        public int UserId { get; private set; }
        public List<int> Products { get; private set; }

        public class Builder
        {
            private int userId;
            private List<int> products = new List<int>();

            public Builder UserId(int userId)
            {
                this.userId = userId;
                return this;
            }

            public Builder Products(List<int> products)
            {
                this.products = products;
                return this;
            }

            public WishVo Build()
            {
                return new WishVo
                {
                    UserId = userId,
                        Products = products
                };
            }
        }
    }
}