namespace luizalabs_wishes_manager.Api.Wish.Model
{
    public class WishResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public class Builder
        {
            private int id;
            private string name;

            public Builder Id(int id)
            {
                this.id = id;
                return this;
            }

            public Builder Name(string name)
            {
                this.name = name;
                return this;
            }

            public WishResponse Build()
            {
                return new WishResponse
                {
                    Id = id,
                        Name = name
                };
            }
        }
    }
}