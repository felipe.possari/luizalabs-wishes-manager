using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.Wish.Model;
using luizalabs_wishes_manager.Api.Wish.Service;
using Microsoft.AspNetCore.Mvc;

namespace luizalabs_wishes_manager.Api.Wish.Controller
{
    [Route("wishes")]
    public class WishController : ControllerBase
    {
        private readonly IWishBuilder iWishBuilder;
        private readonly IWishService iWishService;

        public WishController(IWishBuilder iWishBuilder, IWishService iWishService)
        {
            this.iWishBuilder = iWishBuilder;
            this.iWishService = iWishService;
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<List<WishResponse>>> Get(int userId, int pageSize = 10, int page = 1)
        {
            return Ok(await iWishService.GetWishes(userId, pageSize, page));
        }

        [HttpPost("{userId}")]
        public async Task<ActionResult<WishResponse>> Post([FromBody] List<WishRequest> wishRequest, int userId)
        {
            WishVo wishVo = iWishBuilder.BuildVo(userId, wishRequest);
            var wish = await iWishService.Post(wishVo);
            return CreatedAtAction(nameof(this.Post), wish);
        }

        [HttpDelete("/{userId}/{productId}")]
        public async Task<ActionResult> Delete(int userId, int productId)
        {
            WishVo wishVo = iWishBuilder.BuildVo(userId, productId);
            await iWishService.Delete(wishVo);
            return Ok();
        }
    }
}