using System.Net;

namespace luizalabs_wishes_manager.Api.Wish.Exception
{
    public class WishApiErrorReason
    {
        private readonly string code;
        private readonly string message;
        private readonly HttpStatusCode httpStatusCode;

        public string Code { get { return code; } }
        public string Message { get { return message; } }
        public HttpStatusCode HttpStatusCode { get { return httpStatusCode; } }

        private WishApiErrorReason(string code, string message, HttpStatusCode httpStatusCode)
        {
            this.code = code;
            this.message = message;
            this.httpStatusCode = httpStatusCode;
        }

        public static WishApiErrorReason UserNotFound { get { return new WishApiErrorReason("001", "User not found", HttpStatusCode.NotFound); } }
        public static WishApiErrorReason ProductNotFound { get { return new WishApiErrorReason("002", "Product not found", HttpStatusCode.NotFound); } }
        public static WishApiErrorReason WishExists { get { return new WishApiErrorReason("003", "Wish already registered", HttpStatusCode.BadRequest); } }
        public static WishApiErrorReason WishNotFound { get { return new WishApiErrorReason("004", "Wish not found", HttpStatusCode.NotFound); } }
    }
}