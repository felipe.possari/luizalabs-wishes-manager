using luizalabs_wishes_manager.Exception;
using luizalabs_wishes_manager.Exception.Model;

namespace luizalabs_wishes_manager.Api.Wish.Exception
{
    public class WishApiException : BaseException
    {
        public WishApiException(WishApiErrorReason wishApiErrorReason) : base(
            wishApiErrorReason.Message,
            wishApiErrorReason.Code,
            wishApiErrorReason.HttpStatusCode,
            SystemOperationExceptionCode.WishApi) { }
    }
}