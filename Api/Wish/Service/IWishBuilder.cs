using System.Collections.Generic;
using luizalabs_wishes_manager.Api.Wish.Model;
using luizalabs_wishes_manager.Persistence.Product.Model;
using luizalabs_wishes_manager.Persistence.User.Model;
using luizalabs_wishes_manager.Persistence.Wish.Model;

namespace luizalabs_wishes_manager.Api.Wish.Service
{
    public interface IWishBuilder
    {
         WishVo BuildVo(int userId, List<WishRequest> wishRequest);
         WishVo BuildVo(int userId, int productId);
         List<WishEntity> BuildEntities(UserEntity userEntity, List<ProductEntity> productEntities);
         WishEntity BuildEntity(int userId, int productId);
         List<WishResponse> BuildResponse(List<WishEntity> wishEntities);
         List<WishResponse> BuildResponse(List<ProductEntity> productEntities);
    }
}