using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.Wish.Exception;
using luizalabs_wishes_manager.Api.Wish.Model;
using luizalabs_wishes_manager.Persistence.Product.Model;
using luizalabs_wishes_manager.Persistence.Product.Repository;
using luizalabs_wishes_manager.Persistence.User.Model;
using luizalabs_wishes_manager.Persistence.User.Repository;
using luizalabs_wishes_manager.Persistence.Wish.Model;
using luizalabs_wishes_manager.Persistence.Wish.Repository;

namespace luizalabs_wishes_manager.Api.Wish.Service
{
    public class WishService : IWishService
    {
        private readonly IWishBuilder iWishBuilder;
        private readonly IWishRepository iWishRepository;
        private readonly IProductRepository iProductRepository;
        private readonly IUserRepository iUserRepository;

        public WishService(IWishBuilder iWishBuilder, IWishRepository iWishRepository,
            IProductRepository iProductRepository, IUserRepository iUserRepository)
        {
            this.iProductRepository = iProductRepository;
            this.iWishBuilder = iWishBuilder;
            this.iWishRepository = iWishRepository;
            this.iUserRepository = iUserRepository;
        }

        public async Task<bool> Delete(WishVo wishVo)
        {
            await FindUser(wishVo.UserId);
            await FindProducts(new List<int> { wishVo.Products.First() });

            var wish = await FindWish(wishVo.UserId, wishVo.Products.First());
            if (wish == null)
            {
                throw new WishApiException(WishApiErrorReason.WishNotFound);
            }

            return await iWishRepository.Delete(wish);
        }

        public async Task<List<WishResponse>> GetWishes(int userId, int pageSize, int page)
        {
            var wishes = await iWishRepository.FindWishesByUserId(userId, pageSize, page);
            return iWishBuilder.BuildResponse(wishes);
        }

        public async Task<List<WishResponse>> Post(WishVo wishVo)
        {
            var user = await FindUser(wishVo.UserId);
            var products = await FindProducts(wishVo.Products);

            var wish = await FindWish(wishVo.UserId, wishVo.Products.First());
            if (wish != null)
            {
                throw new WishApiException(WishApiErrorReason.WishExists);
            }

            await iWishRepository.Save(iWishBuilder.BuildEntities(user, products));
            return iWishBuilder.BuildResponse(products);
        }

        private async Task<UserEntity> FindUser(int userId)
        {
            var user = await iUserRepository.FindUserById(userId);
            if (user == null)
            {
                throw new WishApiException(WishApiErrorReason.UserNotFound);
            }
            return user;
        }

        private async Task<List<ProductEntity>> FindProducts(List<int> productIds)
        {
            List<ProductEntity> products = new List<ProductEntity>();
            foreach (int id in productIds)
            {
                var product = await iProductRepository.FindProductById(id);
                if (product == null)
                {
                    throw new WishApiException(WishApiErrorReason.ProductNotFound);
                }
                products.Add(product);
            }
            return products;
        }

        private async Task<WishEntity> FindWish(int userId, int productId)
        {
            return await iWishRepository.FindWishByUserIdAndProductId(userId, productId);
        }
    }
}