using System;
using System.Collections.Generic;
using luizalabs_wishes_manager.Api.Wish.Model;
using luizalabs_wishes_manager.Persistence.Product.Model;
using luizalabs_wishes_manager.Persistence.User.Model;
using luizalabs_wishes_manager.Persistence.Wish.Model;

namespace luizalabs_wishes_manager.Api.Wish.Service
{
    public class WishBuilder : IWishBuilder
    {
        public List<WishEntity> BuildEntities(UserEntity userEntity, List<ProductEntity> productEntities)
        {
            List<WishEntity> wishEntities = new List<WishEntity>();
            productEntities.ForEach(productEntity => wishEntities.Add(BuildEntity(userEntity, productEntity)));
            return wishEntities;
        }

        private WishEntity BuildEntity(UserEntity userEntity, ProductEntity productEntity){
            return new WishEntity.Builder()
                .UserId(userEntity.Id)
                .ProductId(productEntity.Id)
            .Build();
        }

        public WishEntity BuildEntity(int userId, int productId)
        {
            return new WishEntity.Builder()
                .ProductId(productId)
                .UserId(userId)
                .Build();
        }

        public List<WishResponse> BuildResponse(List<WishEntity> wishEntities)
        {
            List<WishResponse> wishes = new List<WishResponse>();
            wishEntities.ForEach(p => wishes.Add(BuildProductResponse(p.Product)));
            return wishes;
        }

        public List<WishResponse> BuildResponse(List<ProductEntity> productEntities)
        {
            List<WishResponse> wishes = new List<WishResponse>();
            productEntities.ForEach(p => wishes.Add(BuildProductResponse(p)));
            return wishes;
        }

        private WishResponse BuildProductResponse(ProductEntity productEntity)
        {
            return new WishResponse.Builder()
                .Id(productEntity.Id)
                .Name(productEntity.Name)
                .Build();
        }

        public WishVo BuildVo(int userId, List<WishRequest> wishRequest)
        {
            return new WishVo.Builder()
                .UserId(userId)
                .Products(BuildProductIds(wishRequest))
                .Build();
        }

        private List<int> BuildProductIds(List<WishRequest> wishRequest){
            List<int> ids = new List<int>();
            wishRequest.ForEach(wish => ids.Add(wish.IdProduct));
            return ids;
        }

        public WishVo BuildVo(int userId, int productId)
        {
            return new WishVo.Builder()
                .UserId(userId)
                .Products(new List<int> { productId })
                .Build();
        }
    }
}