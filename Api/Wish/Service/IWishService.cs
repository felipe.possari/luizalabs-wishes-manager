using System.Collections.Generic;
using System.Threading.Tasks;
using luizalabs_wishes_manager.Api.Wish.Model;

namespace luizalabs_wishes_manager.Api.Wish.Service
{
    public interface IWishService
    {
        Task<List<WishResponse>> Post(WishVo wishVo);
        Task<List<WishResponse>> GetWishes(int userId, int pageSize, int page);
        Task<bool> Delete(WishVo wishVo);
    }
}